defmodule SlowKgWeb.CommentController do
  use SlowKgWeb, :controller

  alias SlowKg.Reactions
  alias SlowKg.Reactions.Comment

  action_fallback SlowKgWeb.FallbackController

  def list_by_post(conn, %{"id" => id}) do
    comments = Reactions.list_comments_by_postID!(id)
    render(conn, "index.json", comments: comments)
  end

  def create_by_post(conn, %{"id" => picture_id, "comment" => body}) do
    comment_params = %{"picture_id" => picture_id, "body" => body}

    with {:ok, %Comment{} = comment} <- Reactions.create_comment(comment_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.comment_path(conn, :list_by_post, picture_id))
      |> render("show.json", comment: comment)
    end
  end

  # Unused
  def index(conn, _params) do
    comments = Reactions.list_comment()
    render(conn, "index.json", comments: comments)
  end

  def create(conn, %{"comment" => comment_params}) do
    with {:ok, %Comment{} = comment} <- Reactions.create_comment(comment_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.comment_path(conn, :list_by_post, comment.picture_id))
      |> render("show.json", comment: comment)
    end
  end

  def update(conn, %{"id" => id, "comment" => comment_params}) do
    comment = Reactions.get_comment!(id)

    with {:ok, %Comment{} = comment} <- Reactions.update_comment(comment, comment_params) do
      render(conn, "show.json", comment: comment)
    end
  end

  def delete(conn, %{"id" => id}) do
    comment = Reactions.get_comment!(id)

    with {:ok, %Comment{}} <- Reactions.delete_comment(comment) do
      send_resp(conn, :no_content, "")
    end
  end
end
