defmodule SlowKgWeb.PictureView do
  use SlowKgWeb, :view
  alias SlowKgWeb.PictureView

  def render("index.json", %{pictures: pictures}) do
    %{pictures: render_many(pictures, PictureView, "picture.json")}
  end

  def render("show.json", %{picture: picture}) do
    render_one(picture, PictureView, "picture.json")
  end

  def render("picture.json", %{picture: picture}) do
    %{id: picture.id, timestamp: picture.inserted_at, comments: length(picture.comments)}
  end
end
