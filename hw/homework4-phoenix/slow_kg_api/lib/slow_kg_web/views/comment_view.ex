defmodule SlowKgWeb.CommentView do
  use SlowKgWeb, :view
  alias SlowKgWeb.CommentView

  def render("index.json", %{comments: comments}) do
    %{comments: render_many(comments, CommentView, "comment.json")}
  end

  def render("show.json", %{comment: comment}) do
    %{timestamp: comment.inserted_at}
  end

  def render("comment.json", %{comment: comment}) do
    %{comment: comment.body, timestamp: comment.inserted_at}
  end
end
