defmodule SlowKgWeb.Router do
  use SlowKgWeb, :router

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/", SlowKgWeb do
    pipe_through(:api)

    get "/pictures", PictureController, :index
    get "/picture/:id", PictureController, :show_image
    delete "/picture/:id", PictureController, :delete
    post "/new-picture-url", PictureController, :create

    get "/comments/:id", CommentController, :list_by_post
    post "/new-comment/:id", CommentController, :create_by_post
  end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/static" do
      pipe_through([:fetch_session, :protect_from_forgery])
      live_dashboard("/dashboard", metrics: SlowKgWeb.Telemetry)
    end
  end
end
