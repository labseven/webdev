defmodule SlowKg.Posts.Picture do
  use Ecto.Schema
  use Arc.Ecto.Schema

  import Ecto.Changeset

  schema "pictures" do
    field :title, :string
    field :image_url, :string

    has_many :comments, SlowKg.Reactions.Comment

    timestamps()
  end

  @doc false
  def changeset(picture, attrs) do
    picture
    |> cast(attrs, [:title, :image_url])
    |> validate_required([:image_url])
  end
end
