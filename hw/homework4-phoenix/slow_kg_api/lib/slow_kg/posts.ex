defmodule SlowKg.Posts do
  @moduledoc """
  The Posts context.
  """

  import Ecto.Query, warn: false
  alias SlowKg.Repo

  alias SlowKg.Posts.Picture

  @doc """
  Returns the list of pictures.

  ## Examples

      iex> list_pictures()
      [%Picture{}, ...]

  """
  def list_pictures do
    Repo.all(from p in Picture, preload: [:comments])
  end

  @doc """
  Gets a single picture.

  Raises `Ecto.NoResultsError` if the Picture does not exist.

  ## Examples

      iex> get_picture!(123)
      %Picture{}

      iex> get_picture!(456)
      ** (Ecto.NoResultsError)

  """
  # def get_picture!(id), do: Repo.get!(Picture, id)
  def get_picture!(id), do: Repo.one!(from p in Picture, where: p.id == ^id, preload: [:comments])

  @doc """
  Creates a picture from url.

  ## Examples

      iex> create_picture_url(%{field: value})
      {:ok, %Picture{}}

      iex> create_picture_url(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_picture(attrs \\ %{}) do
    # Set (empty) comments for rendering response json
    %Picture{comments: []}
    |> Picture.changeset(attrs)
    |> Repo.insert()
    |> download_image()
  end

  defp download_image(pic) do
    with {:ok, picture} <- pic do
      {:ok, resp} =
        :httpc.request(
          :get,
          {picture.image_url, []},
          [],
          body_format: :binary
        )

      {{_, 200, 'OK'}, _headers, body} = resp

      File.write!("./storage/#{picture.id}.jpg", body)

      {:ok, picture}
    end
  end

  @doc """
  Updates a picture.

  ## Examples

      iex> update_picture(picture, %{field: new_value})
      {:ok, %Picture{}}

      iex> update_picture(picture, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_picture(%Picture{} = picture, attrs) do
    picture
    |> Picture.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a picture.

  ## Examples

      iex> delete_picture(picture)
      {:ok, %Picture{}}

      iex> delete_picture(picture)
      {:error, %Ecto.Changeset{}}

  """
  def delete_picture(%Picture{} = picture) do
    delete_image(picture)
    Repo.delete(picture)
  end

  defp delete_image(%Picture{} = picture) do
    File.rm("./storage/#{picture.id}.jpg")
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking picture changes.

  ## Examples

      iex> change_picture(picture)
      %Ecto.Changeset{data: %Picture{}}

  """
  def change_picture(%Picture{} = picture, attrs \\ %{}) do
    Picture.changeset(picture, attrs)
  end
end
