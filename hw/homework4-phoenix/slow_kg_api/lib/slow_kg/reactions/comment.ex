defmodule SlowKg.Reactions.Comment do
  use Ecto.Schema
  import Ecto.Changeset

  schema "comment" do
    field :body, :string
    belongs_to :picture, SlowKg.Posts.Picture

    timestamps()
  end

  @doc false
  def changeset(comment, attrs) do
    comment
    |> cast(attrs, [:body, :picture_id])
    |> validate_required([:body, :picture_id])
  end
end
