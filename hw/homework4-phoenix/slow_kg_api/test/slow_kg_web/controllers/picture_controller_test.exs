defmodule SlowKgWeb.PictureControllerTest do
  use SlowKgWeb.ConnCase

  alias SlowKg.Posts
  alias SlowKg.Posts.Picture

  @create_attrs %{
    image: "some image",
    title: "some title"
  }
  @update_attrs %{
    image: "some updated image",
    title: "some updated title"
  }
  @invalid_attrs %{image: nil, title: nil}

  def fixture(:picture) do
    {:ok, picture} = Posts.create_picture(@create_attrs)
    picture
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all pictures", %{conn: conn} do
      conn = get(conn, Routes.picture_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create picture" do
    test "renders picture when data is valid", %{conn: conn} do
      conn = post(conn, Routes.picture_path(conn, :create), picture: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.picture_path(conn, :show, id))

      assert %{
               "id" => id,
               "image" => "some image",
               "title" => "some title"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.picture_path(conn, :create), picture: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update picture" do
    setup [:create_picture]

    test "renders picture when data is valid", %{conn: conn, picture: %Picture{id: id} = picture} do
      conn = put(conn, Routes.picture_path(conn, :update, picture), picture: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.picture_path(conn, :show, id))

      assert %{
               "id" => id,
               "image" => "some updated image",
               "title" => "some updated title"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, picture: picture} do
      conn = put(conn, Routes.picture_path(conn, :update, picture), picture: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete picture" do
    setup [:create_picture]

    test "deletes chosen picture", %{conn: conn, picture: picture} do
      conn = delete(conn, Routes.picture_path(conn, :delete, picture))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.picture_path(conn, :show, picture))
      end
    end
  end

  defp create_picture(_) do
    picture = fixture(:picture)
    %{picture: picture}
  end
end
