defmodule SlowKg.Repo.Migrations.CreateComment do
  use Ecto.Migration

  def change do
    create table(:comment) do
      add :body, :text
      add :picture_id, references(:pictures, on_delete: :nothing)

      timestamps()
    end

    create index(:comment, [:picture_id])
  end
end
