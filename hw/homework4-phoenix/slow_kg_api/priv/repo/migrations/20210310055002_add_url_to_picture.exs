defmodule SlowKg.Repo.Migrations.AddUrlToPicture do
  use Ecto.Migration

  def change do
    rename table(:pictures), :image, to: :image_url
  end
end
