# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :slow_kg,
  ecto_repos: [SlowKg.Repo]

# Configures the endpoint
config :slow_kg, SlowKgWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "ljeTeTC2B9famvEcaDUR1OF9w74W2K5S3pAV0sCkA7Es8C/9eSVXAng+KUukmBqp",
  render_errors: [view: SlowKgWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: SlowKg.PubSub,
  live_view: [signing_salt: "2WCXqR7W"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
