# SlowKg

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

todo:

* use slugs for saving files and image locations
* Make db count comments in Posts.list_pictures() instead of preloading all comments

commands run:
```shell
mix phx.new slow_kg --no-html --no-webpack

```