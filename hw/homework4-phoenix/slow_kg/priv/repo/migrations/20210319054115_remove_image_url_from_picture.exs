defmodule :"Elixir.SlowKg.Repo.Migrations.Remove imageUrl from picture" do
  use Ecto.Migration

  def change do
    alter table :pictures do
      remove :image_url
    end
  end
end
