defmodule SlowKg.Repo.Migrations.CreatePictures do
  use Ecto.Migration

  def change do
    create table(:pictures) do
      add :title, :string
      add :image_url, :string

      timestamps()
    end

  end
end
