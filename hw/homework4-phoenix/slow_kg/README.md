# SlowKg

A photo-first social media site.

# running
Run with [docker](https://docs.docker.com/get-docker/):
```docker-compose up```

SlowKg will be available at [localhost:8080](http://localhost:8080)

To run commands in the container, run `docker exec -it slow_kg_web_1 bash`. Then, run (for example) `mix phx.routes` to list all server routes

### API
```
GET     /api/pictures                   Get list of pictures
    Response: { pictures: [{ id: id, comments: # of comments, timestamp: date created, title: title of picture},] }
POST    /api/pictures                   Create new picture
    Request:  { image_url: jpg location, title: title of picture (optional) }
    Response: { id: id, comments: # of comments, timestamp: date created, title: title of picture}
GET     /api/pictures/:id               Show picture
    Response: { id: id, comments: # of comments, timestamp: date created, title: title of picture}
DELETE  /api/pictures/:id               Delete picture and associated image

GET     /api/images/:id                 Show jpg of picture

GET     /api/pictures/:pic_id/comments  Get list of picture comments
    Response: { comments: [{comment: body of comment, timestamp: date created },] }

POST    /api/pictures/:pic_id/comments  Create new comment on picture
    Request:  { comment: body of comment }
    Response: { comment: body of comment,timestamp: date created }

```
