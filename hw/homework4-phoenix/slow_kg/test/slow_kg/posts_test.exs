defmodule SlowKg.PostsTest do
  use SlowKg.DataCase

  alias SlowKg.Posts

  describe "pictures" do
    alias SlowKg.Posts.Picture

    @valid_attrs %{image_url: "some image_url", title: "some title"}
    @update_attrs %{image_url: "some updated image_url", title: "some updated title"}
    @invalid_attrs %{image_url: nil, title: nil}

    def picture_fixture(attrs \\ %{}) do
      {:ok, picture} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Posts.create_picture()

      picture
    end

    test "list_pictures/0 returns all pictures" do
      picture = picture_fixture()
      assert Posts.list_pictures() == [picture]
    end

    test "get_picture!/1 returns the picture with given id" do
      picture = picture_fixture()
      assert Posts.get_picture!(picture.id) == picture
    end

    test "create_picture/1 with valid data creates a picture" do
      assert {:ok, %Picture{} = picture} = Posts.create_picture(@valid_attrs)
      assert picture.image_url == "some image_url"
      assert picture.title == "some title"
    end

    test "create_picture/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Posts.create_picture(@invalid_attrs)
    end

    test "update_picture/2 with valid data updates the picture" do
      picture = picture_fixture()
      assert {:ok, %Picture{} = picture} = Posts.update_picture(picture, @update_attrs)
      assert picture.image_url == "some updated image_url"
      assert picture.title == "some updated title"
    end

    test "update_picture/2 with invalid data returns error changeset" do
      picture = picture_fixture()
      assert {:error, %Ecto.Changeset{}} = Posts.update_picture(picture, @invalid_attrs)
      assert picture == Posts.get_picture!(picture.id)
    end

    test "delete_picture/1 deletes the picture" do
      picture = picture_fixture()
      assert {:ok, %Picture{}} = Posts.delete_picture(picture)
      assert_raise Ecto.NoResultsError, fn -> Posts.get_picture!(picture.id) end
    end

    test "change_picture/1 returns a picture changeset" do
      picture = picture_fixture()
      assert %Ecto.Changeset{} = Posts.change_picture(picture)
    end
  end
end
