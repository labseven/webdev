# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :slow_kg,
  ecto_repos: [SlowKg.Repo]

# Configures the endpoint
config :slow_kg, SlowKgWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "OzCdM70gIB/fYep3j7cJ8R36hkvtOvOKUgQN6mIu4LZqPtJ9ECWR8O94nBdf3+6l",
  render_errors: [view: SlowKgWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: SlowKg.PubSub,
  live_view: [signing_salt: "4TTrq0oT"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
