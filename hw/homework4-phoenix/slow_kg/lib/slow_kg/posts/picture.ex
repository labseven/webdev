defmodule SlowKg.Posts.Picture do
  use Ecto.Schema
  import Ecto.Changeset

  schema "pictures" do
    field :title, :string

    field :image_url, :string, virtual: true

    has_many :comments, SlowKg.Reactions.Comment

    timestamps()
  end

  @doc false
  def changeset(picture, attrs) do
    picture
    |> cast(attrs, [:title, :image_url])
    |> validate_required([:image_url])
  end

  def update_changeset(picture, attrs) do
    picture
    |> cast(attrs, [:title, :image_url])
  end
end
