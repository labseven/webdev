defmodule SlowKg.Repo do
  use Ecto.Repo,
    otp_app: :slow_kg,
    adapter: Ecto.Adapters.Postgres
end
