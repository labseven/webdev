defmodule SlowKgWeb.Router do
  use SlowKgWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {SlowKgWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", SlowKgWeb do
    pipe_through :browser

    # live "/", PageLive, :index

    live "/", PictureLive.Index, :index
    live "/pictures/new", PictureLive.Index, :new
    live "/pictures/:id/edit", PictureLive.Index, :edit

    live "/pictures/:id", PictureLive.Show, :show
    live "/pictures/:id/show/edit", PictureLive.Show, :edit
    live "/pictures/:id/comments/new", PictureLive.Show, :new_comment
  end

  scope "/api", SlowKgWeb do
    pipe_through :api

    post "/pictures", PictureController, :create_by_url
    get "/pictures", PictureController, :index
    get "/pictures/:id", PictureController, :show
    delete "/pictures/:id", PictureController, :delete

    get "/images/:id", PictureController, :show_image

    get "/pictures/:pic_id/comments", CommentController, :list_by_pic
    post "/pictures/:pic_id/comments", CommentController, :create_by_pic
  end

  # Other scopes may use custom stacks.
  # scope "/api", SlowKgWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: SlowKgWeb.Telemetry
    end
  end
end
