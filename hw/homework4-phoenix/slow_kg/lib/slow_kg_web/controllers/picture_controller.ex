defmodule SlowKgWeb.PictureController do
  use SlowKgWeb, :controller

  alias SlowKg.Posts
  alias SlowKg.Posts.Picture

  action_fallback SlowKgWeb.FallbackController

  def index(conn, _params) do
    pictures = Posts.list_pictures()
    render(conn, "index.json", pictures: pictures)
  end

  def create_by_url(conn, picture_params) do
    with {:ok, %Picture{} = picture} <- Posts.create_picture(picture_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.picture_path(conn, :show, picture))
      |> render("show.json", picture: picture)
    end
  end

  def show_image(conn, %{"id" => id}) do
    picture = Posts.get_picture!(id)
    send_file(conn, 200, "storage/#{picture.id}.jpg")
  end

  def show(conn, %{"id" => id}) do
    picture = Posts.get_picture!(id)
    render(conn, "show.json", picture: picture)
  end

  def delete(conn, %{"id" => id}) do
    picture = Posts.get_picture!(id)

    with {:ok, %Picture{}} <- Posts.delete_picture(picture) do
      send_resp(conn, :no_content, "")
    end
  end

  # Unused:
  def update(conn, %{"id" => id, "picture" => picture_params}) do
    picture = Posts.get_picture!(id)

    with {:ok, %Picture{} = picture} <- Posts.update_picture(picture, picture_params) do
      render(conn, "show.json", picture: picture)
    end
  end
end
