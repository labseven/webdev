defmodule SlowKgWeb.PictureLive.Show do
  use SlowKgWeb, :live_view

  alias SlowKg.Posts
  alias SlowKg.Reactions.Comment

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    {:noreply,
     socket
     |> assign(:page_title, page_title(socket.assigns.live_action))
     |> assign(:picture, Posts.get_picture!(id))
     |> assign(:comment, %Comment{})}
  end

  defp page_title(:show), do: "Show Picture"
  defp page_title(:edit), do: "Edit Picture"
  defp page_title(:new_comment), do: "New Comment"
end
