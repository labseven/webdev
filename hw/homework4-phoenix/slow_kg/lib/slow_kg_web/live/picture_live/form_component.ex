defmodule SlowKgWeb.PictureLive.FormComponent do
  use SlowKgWeb, :live_component

  alias SlowKg.Posts

  @impl true
  def update(%{picture: picture} = assigns, socket) do
    changeset = Posts.change_picture(picture)

    {:ok,
     socket
     |> assign(assigns)
     |> assign(:changeset, changeset)}
  end

  @impl true
  def handle_event("validate", %{"picture" => picture_params}, socket) do
    changeset =
      socket.assigns.picture
      |> Posts.change_picture(picture_params)
      |> Map.put(:action, :validate)

    {:noreply, assign(socket, :changeset, changeset)}
  end

  def handle_event("save", %{"picture" => picture_params}, socket) do
    save_picture(socket, socket.assigns.action, picture_params)
  end

  defp save_picture(socket, :edit, picture_params) do
    case Posts.update_picture(socket.assigns.picture, picture_params) do
      {:ok, _picture} ->
        {:noreply,
         socket
         |> put_flash(:info, "Picture updated")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, :changeset, changeset)}
    end
  end

  defp save_picture(socket, :new, picture_params) do
    case Posts.create_picture(picture_params) do
      {:ok, _picture} ->
        {:noreply,
         socket
         |> put_flash(:info, "Picture added")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end

  def handle_event("save_comment", %{"comment" => comment_params}, socket) do
    case Reactions.create_comment(comment_params) do
      {:ok, _comment} ->
        {:noreply,
         socket
         |> put_flash(:info, "Comment posted")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end
end
