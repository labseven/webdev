defmodule SlowKgWeb.PictureLive.Index do
  use SlowKgWeb, :live_view

  alias SlowKg.Posts
  alias SlowKg.Posts.Picture

  @impl true
  def mount(_params, _session, socket) do
    {:ok, assign(socket, :pictures, list_pictures())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Picture")
    |> assign(:picture, Posts.get_picture!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Picture")
    |> assign(:picture, %Picture{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Gallery")
    |> assign(:picture, nil)
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    picture = Posts.get_picture!(id)
    {:ok, _} = Posts.delete_picture(picture)

    {:noreply, assign(socket, :pictures, list_pictures())}
  end

  defp list_pictures do
    Posts.list_pictures()
  end
end
