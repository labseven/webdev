//
// HOMEWORK 2
// Due: Monday Feb 22 (23h59)
//
// Put team members + any remarks in a README file that you submit in your final .zip file
//

function roundN (n, i) {
    throw('not implemented')
}

function range (n, m) {
    throw('not implemented')
}

function positive (arr) {
    throw('not implemented')
}

function positiveStr (s) {
    throw('not implemented')
}

function mapStr (s, sep, f) {
    throw('not implemented')
}


const sample = [
    {a: 1,     b: 2,     c: 3},
    {a: 10,    b: 20,    c: 30},
    {a: 99,    b: 66,    c: 33},
    {a: 1,     b: 20,    c: 33},
    {a: 10,    b: 66,    c: 3},
    {a: 99,    b: 2,     c: 30}
]

const sample_obj = {
    a: 33,
    b: 66,
    c: 99,
    x: ['this', 'is', 'a', 'string'],
    y: [1, 2, 3],
    z: []
}

function distinct (objs, field) {
    throw('not implemented')
}

function sort (objs, field) {
    throw('not implemented')
}

function sum (objs, field1, field2) {
    throw('not implemented')
}

function group (objs, field) {
    throw('not implemented')
}

function expand (obj, field) {
    throw('not implemented')
}



class Empty {

    isEmpty() {
	return true
    }

    size() {
	return 0
    }

    height() {
        return 0
    }

    fringe() {
        return []
    }

    preorder() {
        return undefined
    }

    map(f){
        return new Empty()
    }

    trim(){
        return new Empty()
    }
}


class Node {

    constructor(value, left, right) {
	this.value = value
	this.left = left
	this.right = right
    }

    isEmpty() {
	return false
    }

    size() {
	return 1 + this.left.size() + this.right.size()
    }

    height() {
        return 1 + Math.max(this.left.height(), this.right.height())
    }

    fringe() {
        if (this.left.isEmpty() && this.right.isEmpty()) {
            return [this.value]
        }

        return this.left.fringe().concat(this.right.fringe())
    }

    preorder(f){
        f(this.value)
        if (!this.left.isEmpty()) this.left.preorder(f)
        if (!this.right.isEmpty()) this.right.preorder(f)
    }

    map(f){
        return new Node(f(this.value), this.left.map(f), this.right.map(f))
    }

    trim(){
        if (this.left.isEmpty() && this.right.isEmpty()) {
            return new Empty()
        }

        return new Node(this.value, this.left.trim(), this.right.trim())
    }
}

// helper functions
const leaf = (v) => new Node(v, new Empty(), new Empty())
const node = (v, l, r) => new Node(v, l, r)

const sample_tree = node(10,
			 node(20, node(40, leaf(80), leaf(90)),
                                  node(50, leaf(100), leaf(110))),
			 node(30, leaf(60), leaf(70)))
